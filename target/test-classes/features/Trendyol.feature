Feature: Trendyol UI case study

  Scenario: The process has been succesfully completed
    Given User click fancyBoxClose button
    When User click girisYap button
    When User fills "alper1@yaani.com" to email textbox
    When User fills "12866173Ag" to password textbox
    When User click login button
    Then Boutique page displayed
    When User click boutique tabs and verify images is loaded
    Then User select random boutique and verify
    When User select random product
    When User click add to basket button
    Then User checks that the product has been added to the basket