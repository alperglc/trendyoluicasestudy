$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Trendyol.feature");
formatter.feature({
  "line": 1,
  "name": "Trendyol UI case study",
  "description": "",
  "id": "trendyol-uı-case-study",
  "keyword": "Feature"
});
formatter.before({
  "duration": 4952410900,
  "status": "passed"
});
formatter.scenario({
  "line": 3,
  "name": "The process has been succesfully completed",
  "description": "",
  "id": "trendyol-uı-case-study;the-process-has-been-succesfully-completed",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 4,
  "name": "User click fancyBoxClose button",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "User click girisYap button",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "User fills \"alper1@yaani.com\" to email textbox",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "User fills \"12866173Ag\" to password textbox",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "User click login button",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "Boutique page displayed",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "User click boutique tabs and verify images is loaded",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "User select random boutique and verify",
  "keyword": "Then "
});
formatter.step({
  "line": 12,
  "name": "User select random product",
  "keyword": "When "
});
formatter.step({
  "line": 13,
  "name": "User click add to basket button",
  "keyword": "When "
});
formatter.step({
  "line": 14,
  "name": "User checks that the product has been added to the basket",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginSteps.userClickFancyBoxCloseButton()"
});
formatter.result({
  "duration": 785060200,
  "status": "passed"
});
formatter.match({
  "location": "LoginSteps.userClickGirisYapCloseButton()"
});
formatter.result({
  "duration": 1286041900,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "alper1@yaani.com",
      "offset": 12
    }
  ],
  "location": "LoginSteps.userFillsEmailTextBox(String)"
});
formatter.result({
  "duration": 391563800,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "12866173Ag",
      "offset": 12
    }
  ],
  "location": "LoginSteps.userFillsPasswordTextBox(String)"
});
formatter.result({
  "duration": 409215300,
  "status": "passed"
});
formatter.match({
  "location": "LoginSteps.userClickLoginButton()"
});
formatter.result({
  "duration": 2196862800,
  "status": "passed"
});
formatter.match({
  "location": "BoutiqueSteps.loginPage()"
});
formatter.result({
  "duration": 16673900,
  "status": "passed"
});
formatter.match({
  "location": "BoutiqueSteps.userClickBoutiqueTabsAndVerifyImagesIsLoaded()"
});
formatter.result({
  "duration": 11533730300,
  "status": "passed"
});
formatter.match({
  "location": "BoutiqueSteps.userSelectRandomBoutiqueAndVerify()"
});
formatter.result({
  "duration": 1110364900,
  "status": "passed"
});
formatter.match({
  "location": "ProductSteps.userSelectRandomProduct()"
});
formatter.result({
  "duration": 1502637700,
  "status": "passed"
});
formatter.match({
  "location": "ProductSteps.userClickAddProductToBasketButton()"
});
formatter.result({
  "duration": 381219400,
  "status": "passed"
});
formatter.match({
  "location": "ProductSteps.userChecksProductAddedToBasket()"
});
formatter.result({
  "duration": 1095144300,
  "status": "passed"
});
formatter.after({
  "duration": 754013800,
  "status": "passed"
});
});