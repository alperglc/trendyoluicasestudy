package StepDefinitions;

import Pages.HomePage;
import Pages.LoginPage;
import Utilities.PropertiesReader;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginSteps {

	private WebDriver driver = Hooks.driver;
	private WebDriverWait wait;

	public LoginSteps() throws Exception {

		PropertiesReader propertiesReader = new PropertiesReader();
		this.wait = new WebDriverWait(driver, propertiesReader.getTimeout());
	}

	@Given("^User click fancyBoxClose button$")
	public void userClickFancyBoxCloseButton() throws InterruptedException {
		HomePage homePage = new HomePage(driver, wait);
		homePage.closeFancyBox();
	}
	@When("^User click girisYap button$")
	public void userClickGirisYapCloseButton() throws InterruptedException {
		HomePage homePage = new HomePage(driver, wait);
		homePage.clickGirisYapButton();
	}
	@When("^User fills \"([^\"]*)\" to email textbox$")
	public void userFillsEmailTextBox(String email) {
		LoginPage loginPage = new LoginPage(driver,wait);
		loginPage.fillEmailData(email);
	}
	@When("^User fills \"([^\"]*)\" to password textbox$")
	public void userFillsPasswordTextBox(String password) {
		LoginPage loginPage = new LoginPage(driver,wait);
		loginPage.fillPasswordData(password);
	}
	@When("User click login button$")
	public void userClickLoginButton() throws InterruptedException {
		LoginPage loginPage = new LoginPage(driver,wait);
		loginPage.clickLoginButton();
	}
}