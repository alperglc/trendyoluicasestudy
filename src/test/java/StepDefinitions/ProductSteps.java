package StepDefinitions;

import Pages.ProductPage;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ProductSteps {

	private WebDriver driver = Hooks.driver;
	private WebDriverWait wait;

	@When("User select random product")
	public void userSelectRandomProduct(){
		ProductPage productPage = new ProductPage(driver,wait);
		productPage.selectProduct();
	}

	@When("User click add to basket button")
	public void userClickAddProductToBasketButton() {
		ProductPage productPage = new ProductPage(driver,wait);
		productPage.addProductToBasket();

	}
	@Then("User checks that the product has been added to the basket")
	public void userChecksProductAddedToBasket() throws InterruptedException {
		ProductPage productPage = new ProductPage(driver,wait);
		productPage.verifySuccesfullyAddedProduct();
	}
}
