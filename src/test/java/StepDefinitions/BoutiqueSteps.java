package StepDefinitions;

import Pages.BoutiquePage;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BoutiqueSteps {

	private WebDriver driver = Hooks.driver;
	private WebDriverWait wait;

	@Then("Boutique page displayed$")
	public void loginPage() throws InterruptedException {
		BoutiquePage boutiquePage = new BoutiquePage(driver, wait);
		boutiquePage.currentURL();

	}
	@When("User click boutique tabs and verify images is loaded")
	public void userClickBoutiqueTabsAndVerifyImagesIsLoaded() throws InterruptedException {
		BoutiquePage boutiquePage = new BoutiquePage(driver, wait);
		boutiquePage.clickAndVerifyBoutiques();
	}

	@Then("User select random boutique and verify")
	public void userSelectRandomBoutiqueAndVerify() throws InterruptedException {
		BoutiquePage boutiquePage = new BoutiquePage(driver,wait);
		boutiquePage.selectRandomBoutique();
	}

}
