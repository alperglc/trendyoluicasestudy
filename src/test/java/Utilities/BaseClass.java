package Utilities;

import org.apache.commons.lang.math.RandomUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;

public class BaseClass {

	private static WebDriver driver;
	private static WebDriverWait wait;

	public BaseClass(WebDriver driver, WebDriverWait wait) {

		BaseClass.driver = driver;
		BaseClass.wait = wait;
	}

	protected void WaitUntilElementVisible(WebElement element) {

		wait.until(ExpectedConditions.visibilityOf(element));
	}

	public void waitElementClickable(By by) {
		wait.until(elementToBeClickable(by)).click();
	}

	public void clickElement(By element) {
		driver.findElement(element).click();
	}

	public void currentURL() {
		Assert.assertEquals("https://www.trendyol.com/butik/liste/erkek", driver.getCurrentUrl());
	}

	public void isDisplayedElements(By element) throws InterruptedException {
		Thread.sleep(1000);
		boolean result = driver.findElement(element).isDisplayed();
		Assert.assertTrue(result);
	}

	public int getTabsSize(By element) {
		List<WebElement> elements = driver.findElements(element);
		return elements.size();
	}

	public void clickBoutiquesAndVerifyBoutiquesImages(By element, By by){
		int numberOfElement = getTabsSize(element);
		for (int i = 1; i <= numberOfElement; i++) {
			driver.findElement(By.cssSelector("li.tab-link:nth-child(" + i + ")")).click();
			//	((JavascriptExecutor) driver).executeScript("window.scrollTo(250, document.body.scrollHeight)");
			List<WebElement> images = driver.findElements(by);
			isImagesLoaded(images);
		}
	}

	public void isImagesLoaded(List<WebElement> products) {
		for (WebElement image : products) {
			Object result = ((JavascriptExecutor) driver)
					.executeScript("return arguments[0].complete && " + "typeof arguments[0].naturalWidth != \"undefined\" && " + "arguments[0].naturalWidth > 0", image);

			boolean loaded = false;
			if (result instanceof Boolean) {
				loaded = (Boolean) result;
				if (!loaded) {
					System.out.println("Image error: " + image);
				}
			}
		}
	}

	public void clickToRandom(By by) {
		List<WebElement> images = driver.findElements(by);
		images.get(RandomUtils.nextInt(images.size())).click();
	}

}

