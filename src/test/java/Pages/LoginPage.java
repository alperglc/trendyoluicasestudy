package Pages;

import Utilities.BaseClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage extends BaseClass {

	public LoginPage(WebDriver driver, WebDriverWait wait) {

		super(driver, wait);
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "login-email")
	private WebElement emailField;

	@FindBy(id = "login-password-input")
	private WebElement passwordField;

	By loginButton = By.xpath("//body/div[@id='container']/div[@id='login-register']/div[3]/div[1]/form[1]/button[1]/span[1]");
	By notificationCloseButton = By.xpath("//*[@width='13px']");

	public void fillEmailData(String email) {

		WaitUntilElementVisible(emailField);
		emailField.isEnabled();
		emailField.clear();
		emailField.sendKeys(email);
	}

	public void fillPasswordData(String password) {

		WaitUntilElementVisible(passwordField);
		passwordField.isEnabled();
		passwordField.clear();
		passwordField.sendKeys(password);
	}

	public void clickLoginButton() {
		waitElementClickable(loginButton);
		waitElementClickable(notificationCloseButton);
	}

}