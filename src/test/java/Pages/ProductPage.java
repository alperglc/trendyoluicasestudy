package Pages;

import Utilities.BaseClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ProductPage extends BaseClass {

	public ProductPage(WebDriver driver, WebDriverWait wait) {
		super(driver, wait);
	}

	By products = By.xpath("//div[@class='boutique-product']");
	By addToBasketBtn = By.xpath("//button[@class='pr-in-btn add-to-bs']");
	By addToBasketBtnSuccess = By.xpath("//button[@class='pr-in-btn add-to-bs success']");

	public void selectProduct() {
		clickToRandom(products);
	}

	public void addProductToBasket(){
		clickElement(addToBasketBtn);
	}
	public void verifySuccesfullyAddedProduct() throws InterruptedException {
		isDisplayedElements(addToBasketBtnSuccess);
	}
}
