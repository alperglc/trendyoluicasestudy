package Pages;

import Utilities.BaseClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage extends BaseClass {

    public HomePage(WebDriver driver, WebDriverWait wait) {

        super(driver, wait);
        PageFactory.initElements(driver, this);
    }

    By fancyBox = By.className("fancybox-close");
    By girisYap = By.xpath("//p[@class='link-text']");

    public void closeFancyBox() {
        waitElementClickable(fancyBox);
    }

    public void clickGirisYapButton(){
        waitElementClickable(girisYap);
    }

}