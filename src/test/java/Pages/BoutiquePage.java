package Pages;

import Utilities.BaseClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BoutiquePage extends BaseClass {

	public BoutiquePage(WebDriver driver, WebDriverWait wait) {
		super(driver, wait);
	}

	By tabs = By.cssSelector("li.tab-link");
	By boutiques = By.xpath("//article[@class='component-item']");

	public void clickAndVerifyBoutiques() throws InterruptedException {
		clickBoutiquesAndVerifyBoutiquesImages(tabs,boutiques);
	}
	public void selectRandomBoutique(){
		clickToRandom(boutiques);
	}
}
